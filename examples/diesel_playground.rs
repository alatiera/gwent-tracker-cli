extern crate gwent_tracker;
extern crate diesel;

// use gwent_tracker::models;
use gwent_tracker::dbqueries;

// use diesel::prelude::*;

// use gwent_tracker::schema::sessions::dsl::*;


#[allow(unused_variables)]
fn main() {
    let connection = gwent_tracker::establish_connection();

    let latest_session = dbqueries::latest_session(&connection).unwrap();
    let matches_ = dbqueries::session_matches(&connection, &latest_session).unwrap();
    let all_the_sessions = dbqueries::get_sessions(&connection).unwrap();

    println!("{:?}", latest_session);
    // println!("{:#?}", matches_);
    
    for m in matches_{
        println!("{:?}", m);
    }

    for s in all_the_sessions{
        println!("{:?}", s);
    }
    
}