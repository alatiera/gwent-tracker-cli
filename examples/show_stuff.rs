extern crate gwent_tracker;
extern crate diesel;

use gwent_tracker::*;
use self::models::*;
use diesel::prelude::*;
use dbqueries;

fn main() {
    use self::schema::matches::dsl::*;
    use self::schema::sessions::dsl::*;

    let connection = establish_connection();
    let meatches_ = matches.load::<Match>(&connection).expect(
        "Error loading posts",
    );

    let sessions_ = dbqueries::get_sessions(&connection).unwrap();
    // let boxed = Box::new(sessions_);
    let total = gwent_tracker::total_stats(&sessions_);

    println!("{:#?}", &total);


    // for m in matche_ {
    //     println!("----------\n");
    //     println!("ID: {}", m.id);
    //     println!("MMR Diff: {}", m.mmrdiff);
    // }

    // for ses in all_the_sess {
    //     println!("------------");
    //     println!("ID: {}", ses.id);
    //     println!("Wins: {}", ses.wins());
    //     println!("Defeats: {}", ses.defeats());
    //     println!("MMR Diff: {}", ses.mmr());
    // }
}
