ALTER TABLE sessions ADD COLUMN ses_start INTEGER;

ALTER TABLE sessions RENAME TO TempOldTable;
CREATE TABLE `sessions` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`wins`	INTEGER NOT NULL DEFAULT 0,
	`defeats`	INTEGER NOT NULL DEFAULT 0,
	`draws`	INTEGER NOT NULL DEFAULT 0,
	`mmrdiff`	INTEGER NOT NULL DEFAULT 0,
	`winrate`	REAL NOT NULL,
	`ses_start`	INTEGER NOT NULL,
	`last_mod`	INTEGER NOT NULL
);
INSERT INTO sessions(id, wins, defeats, draws, mmrdiff, winrate, ses_start, last_mod)
SELECT id, wins, defeats,draws,mmrdiff,winrate, last_mod,last_mod FROM TempOldTable;
DROP TABLE TempOldTable;