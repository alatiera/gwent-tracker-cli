CREATE TABLE matches (
    id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    draw    BOOLEAN NOT NULL DEFAULT 0,
    mmrdiff INTEGER NOT NULL DEFAULT 0,
    epoch   INTEGER NOT NULL DEFAULT 0,
    session_id  INTEGER NOT NULL
);