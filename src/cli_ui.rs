use std::io::stdin;
use std::error::Error;
// use std::process;

use diesel;
use diesel::prelude::*;

use models::{SessionTrait, NewSession, Session, NewMatch};
use schema;
use dbqueries;

pub fn run(connection: &SqliteConnection) -> Result<(), Box<Error>> {

    // Session load logic
    if let Ok(mut session) = choose_session(connection) {
        // Session tacking loop
        session_loop(connection, &mut session)?;
    }

    // Final Stats on Exit
    let all_sessions = dbqueries::get_sessions(connection)?;
    println!("Weekly Stats:");
    let weekly = ::weekly_stats(connection);
    ::stats_print_monad(&weekly);
    println!("Total Stats:");
    let total = ::total_stats(&all_sessions);
    ::stats_print_monad(&total);
    Ok(())
}


fn choose_session(connection: &SqliteConnection) -> Result<Session, Box<Error>> {

    println!("Do you want to start a new session? [yes/no]:");
    loop {
        let mut question1 = String::new();
        stdin().read_line(&mut question1)?;

        match question1.trim().to_lowercase().as_str() {
            "y" | "yes" => {
                // Doesnt work for sqlite :(
                // let foo = NewSession::new();
                // let bar = diesel::insert(&foo)
                //     .into(schema::sessions::table)
                //     .get_result::<Session>(connection);
                // println!("{:#?}", bar);

                let foo = NewSession::new();
                diesel::insert(&foo).into(schema::sessions::table).execute(
                    connection,
                )?;
                break;
            }
            "n" | "no" => break,
            _ => println!("Please enter a valid option"),
        }
    }
    if let Ok(foo) = dbqueries::latest_session(connection) {
        return Ok(foo);
    } else {
        // This is crap and need refactor.
        let foo = NewSession::new();
        diesel::insert(&foo).into(schema::sessions::table).execute(
            connection,
        )?;
        let bar = dbqueries::latest_session(connection);
        return Ok(bar.unwrap());
    }
}

fn session_loop(connection: &SqliteConnection, session: &mut Session) -> Result<(), Box<Error>> {

    loop {
        // Session Stats
        ::stats_print_monad(session);

        let mut mmr = String::new();

        println!("What was the mmr diff?");
        stdin().read_line(&mut mmr).unwrap();

        // Might be better as a seperate function and a match
        if let Ok(mmr) = mmr.trim().parse() {
            let mmr: i32 = mmr;

            let mut draw = false;
            if mmr < 5 {
                draw = draw_check()?;
            }

            // game archiving logic
            let new_match = NewMatch::new(mmr, draw, session);
            diesel::insert(&new_match)
                .into(schema::matches::table)
                .execute(connection)?;
            session.add_match(&new_match);
            session.save_changes::<Session>(connection)?;

        } else if mmr.trim().to_lowercase() == String::from("quit") {
            break;

        } else {
            println!("Please enter a valid integer")
        }
    }
    Ok(())
}

/// Return true for draw
fn draw_check() -> Result<bool, Box<Error>> {
    let mut d = String::new();
    println!("Was it a draw? [yes/no]:");

    loop {
        stdin().read_line(&mut d)?;
        let d = d.trim().to_lowercase();

        match d.as_str() {
            "y" | "yes" => {
                println!("You draw");
                return Ok(true);
            }
            "n" | "no" => {
                println!("No draw");
                return Ok(false);
            }
            _ => println!("Please enter a valid option"),
        }
    }
}
