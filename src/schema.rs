table! {
    matches (id) {
        id -> Integer,
        draw -> Bool,
        mmrdiff -> Integer,
        epoch -> Integer,
        session_id -> Integer,
    }
}

table! {
    sessions (id) {
        id -> Integer,
        wins -> Integer,
        defeats -> Integer,
        draws -> Integer,
        mmrdiff -> Integer,
        winrate -> Float,
        ses_start -> Integer,
        last_mod -> Integer,
    }
}


pub const SCHEMA: &'static str = r"
    CREATE TABLE IF NOT EXISTS matches (
        id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        draw    BOOLEAN NOT NULL DEFAULT 0,
        mmrdiff INTEGER NOT NULL DEFAULT 0,
        epoch   INTEGER NOT NULL DEFAULT 0,
        session_id  INTEGER NOT NULL
    );

    CREATE TABLE IF NOT EXISTS sessions (
        id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        wins    INTEGER NOT NULL DEFAULT 0,
        defeats INTEGER NOT NULL DEFAULT 0,
        draws   INTEGER NOT NULL DEFAULT 0,
        mmrdiff INTEGER NOT NULL DEFAULT 0,
        winrate REAL NOT NULL,
        ses_start    INTEGER NOT NULL,
        last_mod    INTEGER NOT NULL
    );";