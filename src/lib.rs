#![recursion_limit="128"]
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_codegen;
#[macro_use]
extern crate lazy_static;
extern crate time;
extern crate chrono;
extern crate xdg;

// TODO: rethink the publick api when done prototyping
// most stuff dont need to be publicly exposed
pub mod schema;
pub mod models;
pub mod cli_ui;
pub mod dbqueries;

use std::io::stdin;

use diesel::prelude::*;
use chrono::prelude::*;

use models::SessionTrait;

lazy_static!{
    static ref GWENT_XDG: xdg::BaseDirectories = {
        xdg::BaseDirectories::with_prefix("gwent_tracker").unwrap()
    };
    static ref GWENT_DATA_HOME: std::path::PathBuf = GWENT_XDG.create_data_directory(GWENT_XDG.get_data_home()).unwrap();
    
    static ref DB_PATH: std::path::PathBuf = GWENT_XDG.place_data_file("gwent.db").unwrap();
}

embed_migrations!("migrations/");

pub fn init() -> Result<(), Box<std::error::Error>> {
    &GWENT_DATA_HOME;
    let mut exists = false;
    if let Some(_) = GWENT_XDG.find_data_file("gwent.db") {
        exists = true
    }
    let conn = establish_connection();
    // embedded_migrations::run(&conn)?;
    embedded_migrations::run_with_output(&conn, &mut std::io::stdout())?;

    if !exists {
        import_stats(&conn).unwrap();
    }
    Ok(())
}


pub fn establish_connection() -> SqliteConnection {
    let database_url = DB_PATH.to_str().unwrap();
    // let database_url = &String::from(".random/foo.db");
    SqliteConnection::establish(database_url).expect(&format!(
        "Error connecting to {}",
        database_url
    ))
}


// This is defently a hack, but I cant figure out chrono.
// TODO: use crono types as start/stop inputs instead of i32 epoch.
pub fn weekly_stats(connection: &SqliteConnection) -> models::NewSession {
    let now = Utc::now();
    let now_epoch = now.timestamp() as i32;
    let wday = now.weekday().number_from_monday() as i32;

    let start = now_epoch - wday * 86400; // seconds in a day

    // let monday = now.with_day(now.day() - wday);
    // let monday = chrono::Utc.yo(now.year(), now.day() - wday);
    // let m_epoch = monday.timestamp(); // only works for DateTime, Not Date

    let s = dbqueries::get_timed_sessions(connection, start, now_epoch).unwrap();
    let foo = total_stats(&s);

    foo
}

pub fn total_stats(sessions: &Vec<models::Session>) -> models::NewSession {
    let mut total = models::NewSession::new();

    for session in sessions {
        total.wins += session.wins();
        total.defeats += session.defeats();
        total.draws += session.draws();
        total.mmrdiff += session.mmr();

    }
    total.winrate = total.ratio();
    total
}

// Should Implement this as Display for SessionTrait probably
pub fn stats_print_monad<S: SessionTrait>(session: &S) {
    println!("------------------");
    println!("Victories: {}", session.wins());
    println!("Defeats: {}", session.defeats());
    println!("Draws: {}", session.draws());
    println!("Winrate: {:.2}%", (session.winrate() * 100.0));
    println!("MMR: {}", session.mmr());
    println!("");
}

fn import_stats(db: &SqliteConnection) -> Result<(), Box<std::error::Error>> {
    loop {
        println!("Do you want to impoort your stats? [yes/no]:");
        let mut importq = String::new();
        stdin().read_line(&mut importq).unwrap();

        match importq.trim().to_lowercase().as_str() {
            "y" | "yes" => {

                // This shit will explode horribly but I am too tired to fix it.
                let mut foo = models::NewSession::new();
                let mut w = String::new();
                let mut d = String::new();
                let mut dr = String::new();
                let mut m = String::new();

                println!("Enter your wins:");
                stdin().read_line(&mut w)?;
                let wins: i32 = w.trim().parse()?;

                println!("Enter your defeats:");
                stdin().read_line(&mut d)?;
                let defeats: i32 = d.trim().parse()?;

                println!("Enter your draws:");
                stdin().read_line(&mut dr)?;
                let draws: i32 = dr.trim().parse()?;

                println!("Enter your current mmr:");
                stdin().read_line(&mut m)?;
                let mmr: i32 = m.trim().parse()?;

                foo.wins = wins;
                foo.defeats = defeats;
                foo.draws = draws;
                foo.mmrdiff = mmr;

                diesel::insert(&foo).into(schema::sessions::table).execute(
                    db,
                )?;

                break;
            }
            "n" | "no" => break,
            _ => println!("Please enter a valid option"),
        }
    }
    Ok(())
}