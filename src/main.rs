extern crate gwent_tracker;
use std::io::Write;


// TODO add Season support and season stats


fn main() {
    let conn = gwent_tracker::establish_connection();

    // let now = time::get_time().sec as i32;
    // println!("Epoch: {}", now);
    gwent_tracker::init().expect("Error while initialization.");
    std::process::exit(match gwent_tracker::cli_ui::run(&conn) {
       Ok(_) => 0,
       Err(err) => {
           writeln!(std::io::stderr(), "error: {:?}", err).unwrap();
           1
       }
    });
}