// use diesel::prelude::*;
use schema::{matches, sessions};
use time;


pub trait SessionTrait {
    fn winrate(&self) -> f32;
    fn wins(&self) -> i32;
    fn defeats(&self) -> i32;
    fn draws(&self) -> i32;
    fn mmr(&self) -> i32;
    fn session_start(&self) -> i32;
    fn last_modified(&self) -> i32;
    fn ratio(&self) -> f32;
    fn update_stamp(&mut self);
    fn add_match<M: MatchT>(&mut self, &M);
}

pub trait MatchT {
    fn draw(&self) -> bool;
    fn mmr(&self) -> i32;
    fn epoch(&self) -> i32;
    fn ses_id(&self) -> i32;
}


#[derive(Queryable, Identifiable)]
#[derive(Associations)]
#[table_name = "matches"]
#[belongs_to(Session, foreign_key = "session_id")]
#[derive(Debug)]
pub struct Match {
    id: i32,
    draw: bool,
    mmrdiff: i32,
    epoch: i32,
    session_id: i32,
}


impl MatchT for Match {
    fn draw(&self) -> bool {
        self.draw
    }
    fn mmr(&self) -> i32 {
        self.mmrdiff
    }
    fn epoch(&self) -> i32 {
        self.epoch
    }
    fn ses_id(&self) -> i32 {
        self.session_id
    }
}

#[derive(Insertable)]
#[table_name = "matches"]
pub struct NewMatch {
    draw: bool,
    mmrdiff: i32,
    epoch: i32,
    session_id: i32,
}
impl MatchT for NewMatch {
    fn draw(&self) -> bool {
        self.draw
    }
    fn mmr(&self) -> i32 {
        self.mmrdiff
    }
    fn epoch(&self) -> i32 {
        self.epoch
    }
    fn ses_id(&self) -> i32 {
        self.session_id
    }
}


impl NewMatch {
    pub fn new(mmr: i32, draw: bool, s: &Session) -> NewMatch {
        let foo = NewMatch {
            draw,
            mmrdiff: mmr,
            epoch: time::get_time().sec as i32,
            session_id: s.id,
        };

        foo
    }
}


#[derive(Queryable, Identifiable, AsChangeset)]
#[table_name = "sessions"]
#[derive(Debug, Clone)]
pub struct Session {
    id: i32,
    wins: i32,
    defeats: i32,
    draws: i32,
    mmrdiff: i32,
    winrate: f32,
    ses_start: i32,
    last_mod: i32,
}


impl SessionTrait for Session {
    fn ratio(&self) -> f32 {
        let total = self.wins as f32 + self.defeats as f32 + self.draws as f32;
        if total == 0.0 {
            return 1.0;
        }
        let rate = self.wins as f32 / total;
        return rate;
    }

    fn update_stamp(&mut self) {
        self.last_mod = time::get_time().sec as i32;
    }

    fn add_match<M: MatchT>(&mut self, m: &M) {
        if m.draw() == true {
            self.draws += 1;
        } else if m.mmr() > 0 {
            self.wins += 1;
        } else if m.mmr() <= 0 {
            self.defeats += 1;
        } else {
            // This should never occur
            panic!("Unkown Error");
        }

        self.mmrdiff += m.mmr();
        self.winrate = self.ratio();
        self.update_stamp();
    }

    fn wins(&self) -> i32 {
        self.wins
    }

    fn defeats(&self) -> i32 {
        self.defeats
    }
    fn draws(&self) -> i32 {
        self.draws
    }

    fn mmr(&self) -> i32 {
        self.mmrdiff
    }

    fn winrate(&self) -> f32 {
        self.winrate
    }

    fn session_start(&self) -> i32 {
        self.ses_start
    }
    fn last_modified(&self) -> i32 {
        self.last_mod
    }
}

// TODO: Refthink the pub api of the stucs, maybe set setters
#[derive(Debug)]
#[derive(Insertable)]
#[table_name = "sessions"]
pub struct NewSession {
    pub wins: i32,
    pub defeats: i32,
    pub draws: i32,
    pub mmrdiff: i32,
    pub winrate: f32,
    ses_start: i32,
    last_mod: i32,
}

impl NewSession {
    pub fn new() -> NewSession {
        let t = time::get_time().sec as i32;
        let foo = NewSession {
            wins: 0,
            defeats: 0,
            draws: 0,
            mmrdiff: 0,
            winrate: 0.0,
            ses_start: t,
            last_mod: t,
        };

        foo
    }
}

impl SessionTrait for NewSession {
    fn ratio(&self) -> f32 {
        let total: f32 = self.wins as f32 + self.defeats as f32 + self.draws as f32;
        if total == 0.0 {
            return 1.0;
        }
        let rate: f32 = self.wins as f32 / total;
        return rate;
    }

    fn update_stamp(&mut self) {
        self.last_mod = time::get_time().sec as i32;
    }

    fn add_match<M: MatchT>(&mut self, m: &M) {
        if m.draw() == true {
            self.draws += 1;
        } else if m.mmr() > 0 {
            self.wins += 1;
        } else if m.mmr() <= 0 {
            self.defeats += 1;
        } else {
            // This should never occur
            panic!("Unkown Error");
        }

        self.mmrdiff += m.mmr();
        self.winrate = self.ratio();
        self.update_stamp();
    }

    fn wins(&self) -> i32 {
        self.wins
    }

    fn defeats(&self) -> i32 {
        self.defeats
    }
    fn draws(&self) -> i32 {
        self.draws
    }

    fn mmr(&self) -> i32 {
        self.mmrdiff
    }

    fn winrate(&self) -> f32 {
        self.winrate
    }

    fn session_start(&self) -> i32 {
        self.ses_start
    }

    fn last_modified(&self) -> i32 {
        self.last_mod
    }
}

#[cfg(test)]
mod tests {
    use models::{NewSession, SessionTrait};

    // TODO: Add more tests
    // FIXME: Refactor
    #[test]
    fn winrate_ratio_test() {
        let mut ses1 = NewSession::new();
        ses1.wins = 14;
        ses1.defeats = 9;
        ses1.draws = 2;

        // Usual case
        assert_eq!(ses1.ratio(), 0.56);

        // 0 value case
        let mut ses2 = NewSession::new();
        assert_eq!(ses2.ratio(), 1.0);
    }
}