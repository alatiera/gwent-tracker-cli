use diesel::prelude::*;
use schema::sessions::dsl::*;
// use schema::matches::dsl::*;
use models::{Session, Match};

pub fn latest_session(connection: &SqliteConnection) -> QueryResult<Session> {

    let latest_ses = sessions
        .limit(1)
        .order(last_mod.desc())
        .get_result::<Session>(connection);

    latest_ses
}

pub fn session_matches(connection: &SqliteConnection, ses: &Session) -> QueryResult<Vec<Match>> {
    let ses_matches = Match::belonging_to(ses).load::<Match>(connection);

    ses_matches
}

pub fn get_sessions(connection: &SqliteConnection) -> QueryResult<Vec<Session>> {
    let s = sessions.load::<Session>(connection);

    s
}

pub fn get_timed_sessions(
    connection: &SqliteConnection,
    start: i32,
    stop: i32,
) -> QueryResult<Vec<Session>> {

    let s = sessions
        .filter(ses_start.ge(start))
        .filter(last_mod.le(stop))
        .load::<Session>(connection);

    // println!("Sessions: {:#?}", s);

    s
}