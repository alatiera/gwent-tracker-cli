# Gwent-Tracker-cli
## A crappy cli utility to track your gwent matches, that barely works.

### Abandoned.
I kinda stopped playing the game once the proladder was released.

400 games per month was a big commitment for me.

The tracker was built for the normal ranked ladder, and never got around to
adapting it for the PRO-ladder.

### Local Build:
```
git clone https://github.com/alatiera/gwent-tracker-cli.git
cd gwent-tracker-cli
cargo install --path . --force # make sure .cargo/bin is in your path or copy the binary to it
gwent_tracker
```
